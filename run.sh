#!/bin/bash

path=/home/moabson/debugging-ml
python_interpreter=python3

repo_tool_py=/home/moabson/eclipse-workspace-python/ml-debugging-repo-tool/main.py
analyzer_tool_py=/home/moabson/eclipse-workspace-python/ml-debugging-analyzer-tool/main.py
finder_tool_jar=/home/moabson/eclipse-workspace-java/ml-debugging-find-tool/finder.jar
finder_tool_keywords=/home/moabson/eclipse-workspace-java/ml-debugging-find-tool/data/terms.json


create_dirs() {
	mkdir -p $path
	mkdir -p $path/input
	mkdir -p $path/indexes
	mkdir -p $path/output
	mkdir -p $path/output/repo-tool
	mkdir -p $path/output/find-tool
	mkdir -p $path/output/analyzer-tool
}

start() {
	$python_interpreter	$repo_tool_py $path
	# java -jar $finder_tool_jar -createIndexes --inputDir $path/input --indexDir $path/indexes --keywords $finder_tool_keywords --output1 $path/output/find-tool/result.json --output2 $path/output/find-tool/result_files.json
}

create_dirs
start


 


